package com.luv2code.springdemo;

public class ToughFortuneService implements FortuneService {

	@Override
	public String getFortune() {
		return "Still haven't done enough...";
	}

}
