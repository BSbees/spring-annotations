package com.luv2code.springdemo;

public class SquashCoach implements Coach {

	private FortuneService fortuneService;
	
	public SquashCoach(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "Squashing ball another 15 min";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

}
