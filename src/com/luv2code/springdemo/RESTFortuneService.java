package com.luv2code.springdemo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RESTFortuneService implements FortuneService {

	@Value("${foo.text}")
	private String fortune;
	
	@Override
	public String getFortune() {
		return fortune;
	}

}
