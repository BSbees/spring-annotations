package com.luv2code.springdemo;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class DatabaseFortuneService implements FortuneService {
	private String[] data = {"fortune 1", "fortune 2", "fortune 3"};
	private Random r = new Random();

	@Override
	public String getFortune() {
		return data[r.nextInt(data.length)];
	}

}
