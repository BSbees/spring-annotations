package com.luv2code.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class MathCoach implements Coach {
	private FortuneService fortuneService;

	@Autowired
	public MathCoach(@Qualifier("sadFortuneService") FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "In a Math Workout method";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

}
