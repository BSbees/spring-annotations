package com.luv2code.springdemo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.annotation.PostConstruct;

public class SadFortuneService implements FortuneService {

	private List<String> fortunes = new ArrayList<>();
	@PostConstruct
	public void createSadFortune(){
		try {
			Scanner sc = new Scanner(new File("src/fortunes.txt"));
			while(sc.hasNext())
				fortunes.add(sc.nextLine());
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
			e.printStackTrace();
		}
		
	}
	@Override
	public String getFortune() {
		Random r = new Random();
		return fortunes.get(r.nextInt(2));
	}

}
