package com.luv2code.springdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SquashConfig {

	@Bean
	public FortuneService toughFortuneService(){
		return new ToughFortuneService();
	}
	
	@Bean
	public Coach squashCoach(){
		return new SquashCoach(toughFortuneService());
	}
}
