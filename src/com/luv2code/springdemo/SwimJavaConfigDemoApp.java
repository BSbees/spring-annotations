package com.luv2code.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SwimJavaConfigDemoApp {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SquashConfig.class);
		Coach theCoach = context.getBean("squashCoach", Coach.class);
		System.out.println(theCoach.getDailyWorkout());	
		System.out.println(theCoach.getDailyFortune());
		
		context.close();
	}

}
